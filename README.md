# CodeMash 2020

Talks given by Joe Sewell at [CodeMash][codemash] 2020.

* [Support Emoji 💁‍♀️ and Go Worldwide 🌐 with Unicode][emoji-unicode-abstract]
  * [Presentation][emoji-unicode-slides], including speaker notes
  * [Sample code][emoji-unicode-code]
* [Assembly Language: The (Sega) Genesis of Programming][genesis-assembly-abstract]
  * [Video][genesis-assembly-video] on Pluralsight (no account required)
  * [Presentation][genesis-assembly-slides], including speaker notes
  * [Sample code][genesis-assembly-code]
  * [ROM image][genesis-assembly-rom]

Speaker information:

* [CodeMash profile][speaker]
* [Twitter][twitter]
* [LinkedIn][linkedin]

[codemash]: https://www.codemash.org/
[speaker]: https://www.codemash.org/speaker-details/?id=74886f64-2f11-4c70-abe8-65d1a1a5de4d
[twitter]: https://twitter.com/JoeDotDeveloper
[linkedin]: https://www.linkedin.com/in/joseph-sewell-23162a52/

[emoji-unicode-abstract]: https://www.codemash.org/session-details/?id=145601
[emoji-unicode-slides]: https://docs.google.com/presentation/d/1OQt7T37PXk_nPdurpB7ISNS97veh5-spwcOJLK0ajfI/edit?usp=sharing
[emoji-unicode-code]: https://gitlab.com/Joe-K-Sewell/unicode

[genesis-assembly-abstract]: https://www.codemash.org/session-details/?id=145606
[genesis-assembly-video]: https://www.pluralsight.com/courses/codemash-session-97
[genesis-assembly-slides]: https://docs.google.com/presentation/d/1s5fN3x3Z8sZjxKsu0RDSKzp1bMBEp-1a9MEtawF29QA/edit?usp=sharing
[genesis-assembly-code]: https://gitlab.com/Joe-K-Sewell/genesis
[genesis-assembly-rom]: https://gitlab.com/Joe-K-Sewell/genesis/blob/codemash-2020/CodeMash.BIN